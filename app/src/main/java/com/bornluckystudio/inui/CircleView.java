package com.bornluckystudio.inui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

public class CircleView extends View {

    private AttributeSet attributeSet;

    private Boolean active;
    private int activeColor;
    private int deactiveCloror;

    public CircleView(Context context) {
        this(context, null);
    }

    public CircleView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        this.attributeSet = attrs;

        config();
    }

    public void setActive(boolean active) {
        this.active = active;

        changeColor();
    }

    private void config() {
        TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(this.attributeSet,
                R.styleable.CircleView,
                0, 0);

        try {
            this.active = typedArray.getBoolean(R.styleable.CircleView_active, false);
            this.activeColor = typedArray.getColor(R.styleable.CircleView_activeColor, 0);
            this.deactiveCloror = typedArray.getColor(R.styleable.CircleView_deactiveColor, 0);

        } finally {
            typedArray.recycle();
        }

        changeColor();
    }

    private void changeColor() {
        setBackground(ContextCompat.getDrawable(getContext(), R.drawable.circle));
        GradientDrawable gradientDrawable = (GradientDrawable)this.getBackground();

        gradientDrawable.setColor(this.active ?  activeColor : deactiveCloror);
    }
}
